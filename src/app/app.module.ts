import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { CalendarModule } from './calendar/calendar.module';

import { AppComponent } from './app.component';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule, CalendarModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }