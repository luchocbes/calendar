import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    month: number;
    year: number;

    ngOnInit() {
        let date = new Date();

        this.month = date.getMonth() + 1;
        this.year = date.getFullYear();
    }
}