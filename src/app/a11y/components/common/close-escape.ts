import { NgModule, Injectable, ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';

@Injectable()
export class CloseOnEscape {
    public set(el: ElementRef = undefined, callback: Function = undefined, stopPropagation: boolean = false) {
        if (el.nativeElement && el.nativeElement.addEventListener) {
            el.nativeElement.addEventListener('keydown', (e) => this.closeEscape(e, callback, stopPropagation));
        } else if (el.nativeElement && el.nativeElement.attachEvent) {
            el.nativeElement.attachEvent('keydown', (e) => this.closeEscape(e, callback, stopPropagation));
        }
    }

    block(el: ElementRef = undefined) {
        this.set(el, undefined, true);
    }

    closeEscape(e, callback, block: boolean = false) {
        let keyCode = e.which || e.keyCode;
        if (keyCode === 27) {
            e.preventDefault();
            if (!block) {
                if (callback) callback();
            } else {
                e.stopPropagation();
            }
        }
    }
}

@NgModule({
    declarations: [],
    providers: [CloseOnEscape],
    imports: [CommonModule],
    exports: []
})
export class CloseOnEscapeModule { }