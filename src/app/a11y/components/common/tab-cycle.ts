import { NgModule, Injectable, ElementRef } from '@angular/core';
import { CommonModule } from '@angular/common';

@Injectable()
export class TabCycle {
    set(el: ElementRef = undefined) {
        if (el.nativeElement && el.nativeElement.addEventListener) {
            el.nativeElement.addEventListener('keydown', (e) => this.tabCycle(e, el.nativeElement));
        } else if (el.nativeElement && el.nativeElement.attachEvent) {
            el.nativeElement.attachEvent('keydown', (e) => this.tabCycle(e, el.nativeElement));
        }
    }

    remove(el: ElementRef = undefined) {
        if (el.nativeElement && el.nativeElement.removeEventListener) {
            el.nativeElement.removeEventListener('keydown', (e) => this.tabCycle(e, el.nativeElement));
        } else if (el.nativeElement && el.nativeElement.detachEvent) {
            el.nativeElement.detachEvent('keydown', (e) => this.tabCycle(e, el.nativeElement));
        }
    }

    tabCycle(e, el) {
        const tabbableElements = 'a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]';
        var allTabbableElements = el.querySelectorAll(tabbableElements);

        if (allTabbableElements.length) {
            var firstTabbableElement = allTabbableElements[0];
            var lastTabbableElement = allTabbableElements[allTabbableElements.length - 1];

            var keyCode = e.which || e.keyCode;

            if (keyCode === 9) {
                e.stopPropagation();
                if (e.target === lastTabbableElement && !e.shiftKey) {
                    e.preventDefault();
                    firstTabbableElement.focus();
                } else if (e.target === firstTabbableElement && e.shiftKey) {
                    e.preventDefault();
                    lastTabbableElement.focus();
                }
            }
        } else {
            // No tabbable elements
        }
    }
}

@NgModule({
    declarations: [],
    providers: [TabCycle],
    imports: [CommonModule],
    exports: []
})
export class TabCycleModule { }