import { NgModule, Pipe, PipeTransform } from '@angular/core';
import { CommonModule } from '@angular/common';

@Pipe({ name: 'a11yStripHTML' })
export class StripHTMLPipe implements PipeTransform {
    constructor() { }

    transform(value: string): String {
        return (value !== undefined) ? value.replace(/<(.*?)>/ig, '') : '';
    }
}

@NgModule({
    declarations: [StripHTMLPipe],
    imports: [CommonModule],
    exports: [StripHTMLPipe]
})
export class StripHTMLPipeModule { }