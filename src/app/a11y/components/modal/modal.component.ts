import { Component, Input, Output, ViewChild, EventEmitter, OnInit, AfterViewInit, Renderer2 } from '@angular/core';

import { trigger, state, style, animate, transition } from '@angular/animations';

import { TabCycle } from '../common/tab-cycle';
import { CloseOnEscape } from '../common/close-escape';

import { a11y } from '../../index';
import { ModalTranslation, defaultValues, ModalCloseOn, ModalButtons } from '../../translations/modal';

declare var require: any;

let modals: string[] = [];

let modalUniqueID = 0;
let zIndexInit = 1050;
let scrollbarWidth = undefined;

@Component({
    selector: 'a11y-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss'],
    animations: [
        trigger('fadeBackdrop', [
            state('1', style({ opacity: '.6', display: 'block' })),
            state('0', style({ opacity: '0', display: 'none' })),
            transition('* => *', animate('250ms ease-in-out'))
        ]),
        trigger('modalOpen', [
            state('1', style({ opacity: '1', marginTop: '1.75rem' })),
            state('0', style({ opacity: '0', marginTop: '-1.75rem' })),
            transition('* => *', animate('250ms ease-in-out'))
        ]),
        trigger('modalOpenDisplay', [
            state('1', style({ display: 'block' })),
            state('0', style({ display: 'none' })),
            transition('0 => 1', animate('0ms ease')),
            transition('1 => 0', animate('0ms 250ms ease'))
        ])
    ]
})
export class ModalComponent implements OnInit, AfterViewInit {
    @Input() modalTitle: string;
    @Input() closeOn: ModalCloseOn;
    @Input() allowClose: boolean = true;
    @Input() disableClose: boolean = false;
    @Input() width: string;
    @Input() buttons: ModalButtons[] = [];

    @Input() labels: ModalTranslation;

    @ViewChild('a11yModalWrapper') a11yModalWrapper;
    @ViewChild('a11yModal') a11yModal;

    @ViewChild('scrollbarCalcWrapper') scrollbarCalcWrapper;
    @ViewChild('scrollbarCalc') scrollbarCalc;

    @Output('onClose') modalEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

    backdropClick(e) {
        if (this.modalOpen) {
            if (this.closeOn.escape) {
                this.close();
            } else {
                this.a11yModal.nativeElement.focus();
            }
        }
    }

    modalID = `a11y-modal-${modalUniqueID++}`;
    zIndex;

    modalOpen: boolean = false;
    modalVisible: boolean = false;
    triggerer: any;

    modalFullHeight: number;
    scrollbarFullWidth: number;

    get scrollbarWidth(): number {
        return this.scrollbarFullWidth;
    }
    set scrollbarWidth(width: number) {
        setTimeout(() => { this.scrollbarFullWidth = width; }, 0);
    }

    setScrollbarWidth() {
        scrollbarWidth = (this.scrollbarCalc.nativeElement.clientWidth - this.scrollbarCalcWrapper.nativeElement.clientWidth);
        this.scrollbarWidth = scrollbarWidth;
    }

    get backdropRightOffset(): string {
        return (this.modalFullHeight && window.innerHeight < this.modalFullHeight) ? this.scrollbarWidth + 'px' : '0px';
    }

    get modalIndexPosition(): number {
        return modals.indexOf(this.modalID);
    }

    getModalFullHeight() {
        let styles = document.defaultView.getComputedStyle(this.a11yModal.nativeElement);
        let height = Number(styles.height.replace('px', ''));
        let margins = Number(styles.marginTop.replace('px', '')) + Number(styles.marginBottom.replace('px', ''));
        this.modalFullHeight = (height + margins);
    }

    open(e: MouseEvent) {
        setTimeout(() => {
            if (modals.indexOf(this.modalID) === -1) {
                this.triggerer = (e && e.target) ? e.target : (e) ? e : document.activeElement;
    
                this.modalOpen = true;
                this.modalVisible = true;
                this.renderer.addClass(document.body, 'modal-open');
                modals.push(this.modalID);
                this.zIndex = zIndexInit + (this.modalIndexPosition + 1);

                setTimeout(() => {
                    this.getModalFullHeight();
                    this.a11yModal.nativeElement.focus();
                    this.a11yModalWrapper.nativeElement.scrollTo(0, 0);
                }, 255);
            }
        }, 0);
    }

    close() {
        if (!this.disableClose) {
            setTimeout(() => {
                modals.splice(this.modalIndexPosition, 1);
                this.zIndex = undefined;
                this.modalOpen = false;
                if (this.triggerer) this.triggerer.focus();
            }, 0);
            setTimeout(() => {
                this.modalEmitter.emit(true);
                this.modalVisible = false;
                if (!modals.length) this.renderer.removeClass(document.body, 'modal-open');
            }, 255);
        }
    }

    constructor(private tabCycle: TabCycle, private closeOnEscape: CloseOnEscape, private renderer: Renderer2) { };

    ngOnInit() {
        let a11yLang = a11y.langDefault;
        let translation: ModalTranslation = require(`../../translations/modal.${a11yLang}.json`);
        translation = { ...translation, ...defaultValues };
        this.labels = { ...translation, ...this.labels };

        if (!this.allowClose && this.buttons.length > 0) {
            this.closeOn = { escape: false, closeButton: false, dismiss: false, cancel: false };
        } else {
            this.closeOn = { ...{ escape: true, closeButton: true, dismiss: true, cancel: false }, ...this.closeOn };
        }

        if (this.closeOn.escape) {
            this.closeOnEscape.set(this.a11yModal, () => this.close());
        } else {
            this.closeOnEscape.block(this.a11yModal);
        }
    }

    ngAfterViewInit() {
        if (!scrollbarWidth) this.setScrollbarWidth();

        this.tabCycle.set(this.a11yModalWrapper);
    }
}
