import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ModalComponent } from './modal.component';

import { TabCycleModule } from '../common/tab-cycle';
import { CloseOnEscapeModule } from '../common/close-escape';
import { StripHTMLPipeModule } from '../common/strip-html.pipe';
import { SafePipeModule } from '../common/safe.pipe';

@NgModule({
	declarations: [ModalComponent],
	imports: [CommonModule, TabCycleModule, CloseOnEscapeModule, StripHTMLPipeModule, SafePipeModule, BrowserAnimationsModule],
	providers: [],
	exports: [ModalComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class A11yModal { }
