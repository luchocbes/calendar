export interface ModalCloseOn {
    escape?: Boolean;
    closeButton?: Boolean;
    dismiss?: Boolean;
    cancel?: Boolean;
}

export interface ModalButtons {
    buttonText: String;
    buttonClass?: String;
    buttonDisabled?: Boolean;
    buttonCallback: Function;
}

export interface ModalTranslation {
    close?: String;
    cancel?: String;
    dismiss?: String;
};

export const defaultValues: ModalTranslation = {
    
};