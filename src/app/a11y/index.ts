 export interface A11yInterface {
    langSupported?: string[];
    langDefault?: string;
};

export const a11y: A11yInterface = {
    langSupported: ['EN', 'ES'],
    langDefault: 'EN'
};