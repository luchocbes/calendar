import { Component, Input } from '@angular/core';

import { Reminder } from './types';
import { CalendarComponent } from './calendar.component';

@Component({
    selector: 'reminder',
    templateUrl: './reminder.component.html',
    styleUrls: ['./reminder.component.scss']
})
export class ReminderComponent {
    @Input() reminder: Reminder;

    constructor(private cal: CalendarComponent) { }

    getTime(date: Date) {
        date = new Date(date);
        let hTemp = date.getHours();
        let a = true;

        if (hTemp === 0) {
            hTemp = 12;
        } else if (hTemp >= 12) {
            a = false;
            if (hTemp > 12) {
                hTemp -= 12;
            }
        }
        let h = ('0' + hTemp).slice(-2);
        let m = ('0' + date.getMinutes()).slice(-2);

        return `${h}:${m} ${(a) ? 'AM' : 'PM'}`;
    }

    viewReminder() {
        this.cal.viewReminder(this.reminder);
    }
}