import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';

import { City } from './types';

@Injectable()
export class WeatherService {
    private weather_api: string = 'b908405dcc45101524645ceec9d3dc30';

    constructor(private http: HttpClient) { }

    cities: City[] = [
        {
            "id": 3689147,
            "country": "CO",
            "name": "Barranquilla",
        },
        {
            "id": 3688689,
            "country": "CO",
            "name": "Bogota",
        },
        {
            "id": 3435910,
            "country": "AR",
            "name": "Buenos Aires",
        },
        {
            "id": 3860259,
            "country": "AR",
            "name": "Cordoba",
        },
        {
            "id": 292223,
            "country": "AE",
            "name": "Dubai",
        },
        {
            "id": 3117735,
            "country": "ES",
            "name": "Madrid",
        },
        {
            "id": 4164138,
            "country": "US",
            "name": "Miami",
        },
        {
            "id": 5106292,
            "country": "US",
            "name": "New York",
        },
        {
            "id": 3451190,
            "country": "BR",
            "name": "Rio de Janeiro",
        },
        {
            "id": 3169070,
            "country": "IT",
            "name": "Roma",
        },
        {
            "id": 5549222,
            "country": "US",
            "name": "Washington",
        }
    ];

    getCities() {
        return this.cities;
    }

    getWeather(cityId: number) {
        return this.http.get(`https://api.openweathermap.org/data/2.5/weather?id=${cityId}&APPID=${this.weather_api}`);
    }
}