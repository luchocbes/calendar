import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";

export const WEEKDAYS: string[] = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
export const DAYS_IN_MONTH: number[] = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
export const MONTHS: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
export const YEAR_MIN: number = 1900;
export const YEAR_MAX: number = 2100;

@Component({
    selector: 'datetime',
    templateUrl: './datetime.component.html',
    styleUrls: ['./datetime.component.scss']
})
export class DateTimeComponent implements OnInit {
    @Input() date: Date;

    @Output('changed') changed: EventEmitter<Date> = new EventEmitter<Date>();

    days: number[];
    hours: number[];
    minutes: number[];

    dateSplitted: { y: number, m: number, d: number, h: number, n: number, a: boolean }

    ngOnInit() {
        this.dateSplitted = {
            y: this.date.getFullYear(),
            m: this.date.getMonth(),
            d: this.date.getDate(),
            h: this.date.getHours(),
            n: this.date.getMinutes(),
            a: true
        };

        if (this.dateSplitted.n > 55) {
            this.dateSplitted.n = 0;
            this.dateSplitted.h = 12;
        } else {
            if (this.dateSplitted.h === 0) {
                this.dateSplitted.h = 12;
            } else if (this.dateSplitted.h > 12) {
                this.dateSplitted.h = (this.dateSplitted.h - 12);
                this.dateSplitted.a = false;
            }
        }

        this.getDaysInMonth();
        this.hours = new Array(13).fill((h, i) => { return i }).map((h, i) => { return i });
        this.hours.shift();
        this.minutes = new Array(12).fill((h, i) => { return i }).map((h, i) => { return (i * 5) });
    }

    getDaysInMonth() {
        this.checkLeapYear();
        this.days = new Array(DAYS_IN_MONTH[this.dateSplitted.m]).fill((h, i) => { return i }).map((h, i) => { return (i + 1) });

        if (this.dateSplitted.d > DAYS_IN_MONTH[this.dateSplitted.m]) {
            this.dateSplitted.d = DAYS_IN_MONTH[this.dateSplitted.m];
        }

        this.saveDate();
    }

    saveDate() {
        this.dateSplitted.m = Number(this.dateSplitted.m);
        this.dateSplitted.d = Number(this.dateSplitted.d);
        this.dateSplitted.n = Number(this.dateSplitted.n);
        this.dateSplitted.a = String(this.dateSplitted.a) === 'true';

        let h: number = Number(this.dateSplitted.h);
        if (h === 12 && this.dateSplitted.a) {
            h = 0;
        } else if (h < 12 && !this.dateSplitted.a) {
            h += 12;
        }

        let date = new Date(this.dateSplitted.y, this.dateSplitted.m, this.dateSplitted.d, h, this.dateSplitted.n, 0);
        this.changed.emit(date);
    }

    get isLeapYear(): boolean {
        return ((this.dateSplitted.y % 4 === 0) && (this.dateSplitted.y % 100 !== 0)) || (this.dateSplitted.y % 400 === 0);
    }

    checkLeapYear() {
        if (Number(this.dateSplitted.m) === 1) {
            DAYS_IN_MONTH[1] = (this.isLeapYear) ? 29 : 28;
        }
    }

    get months(): string[] {
        return MONTHS;
    }

    getHour(h: number): string {
        return ('0' + h).slice(-2);
    }

    getMinutes(m: number): string {
        return ('0' + m).slice(-2);
    }
}