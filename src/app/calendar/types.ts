export const COLORS: string[] = ['#91ffc7', '#faffab', '#baffef', '#ffbaba', '#ffeaba', '#badfff', '#efbaff'];

export interface Reminder {
    text: string;
    cityId: number;
    date: Date;
    color: string;
    created: number;
}

export interface City {
    id: number;
    name: string;
    country: string;
}