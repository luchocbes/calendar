import { Component, OnInit, Input } from '@angular/core';

import { COLORS, Reminder, City } from './types';
import { WeatherService } from './weather.service';
import { StorageService } from './storage.service';

@Component({
    selector: 'create-edit-reminder',
    templateUrl: './create-edit.component.html',
    styleUrls: ['./create-edit.component.scss']
})
export class CreateEditModalComponent implements OnInit {
    @Input() data: Reminder;

    originalData: Reminder;

    isNew: boolean;
    isEditing: boolean;
    submitted: boolean = false;

    originalDate: Date;

    constructor(private ws: WeatherService, private ss: StorageService) { }

    get cities(): City[] {
        return this.ws.getCities();
    }

    get colors(): string[] {
        return COLORS;
    }

    get dateError(): boolean {
        if (this.submitted) {
            let now: number = (new Date()).getTime();
            return (this.data.date.getTime() < now);
        }

        return false;
    }

    get cityError(): boolean {
        if (this.submitted) {
            return Number(this.data.cityId) === -1;
        }

        return false;
    }

    get textError(): boolean {
        if (this.submitted) {
            return this.data.text.length < 3 || this.data.text.length > 30;
        }

        return false;
    }

    ngOnInit() {
        this.init();
    }

    init() {
        if (!this.data.created) {
            /* Set date of new reminder for next hour */
            let date = new Date();
                date = new Date(date.getTime() + (60 * 60 * 1000));
                date = new Date(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), 0, 0);

            this.data = {
                cityId: -1,
                color: COLORS[0],
                date: date,
                text: '',
                created: 0
            };

            this.isNew = true;
        } else {
            this.originalData = { ...this.data };
            this.originalDate = this.originalData.date;
            this.isNew = false;
        }
    }

    saveReminder() {
        this.submitted = true;
        let valid: boolean = !(this.dateError || this.cityError || this.textError);

        if (valid) {
            if (!this.isNew) {
                this.ss.removeReminder(this.originalData);
            } else {
                this.data.created = (new Date()).getTime();
            }

            this.data.cityId = Number(this.data.cityId);
            this.ss.saveReminder(this.data);
        }

        return valid;
    }
}