import { Injectable } from "@angular/core";
import { BehaviorSubject } from 'rxjs';

import { Reminder } from './types';

@Injectable()
export class StorageService {
    reminders: Object = {};

    remindersChanged = new BehaviorSubject<string>(undefined);

    constructor() {
        this.reminders = this.readStorage;
    }

    getReminders(date: string): {} {
        return this.reminders[date] || {};
    }

    getReminder(date: Date): Reminder {
        return undefined;
    }

    saveReminder(reminder: Reminder) {
        let date = this.dateFormat(reminder.date);
        let day = reminder.date.getDate();

        if (!this.reminders.hasOwnProperty(date)) {
            this.reminders[date] = {};
        }

        if (!this.reminders[date].hasOwnProperty(day)) {
            this.reminders[date][day] = [];
        }

        this.reminders[date][day].push(reminder);
        this.writeStorage(date);
    }

    removeReminder(reminder: Reminder) {
        let date = this.dateFormat(reminder.date);
        let day = reminder.date.getDate();

        let idx = this.reminders[date][day].findIndex((r: Reminder) => r.created === reminder.created);
        this.reminders[date][day].splice(idx, 1);
        this.writeStorage(date);
    }

    deleteAll(date: Date) {
        let dateTmp: string = this.dateFormat(date);
        let day = date.getDate();

        this.reminders[dateTmp][day] = undefined;
        this.writeStorage(dateTmp);
    }

    get readStorage(): Object {
        let storage = JSON.parse(localStorage.getItem('reminders')) || {};

        for (let date in storage) {
            for (let day in storage[date]) {
                storage[date][day].forEach((r: Reminder) => {
                    r.date = new Date(r.date);
                    r.cityId = Number(r.cityId);
                });
            }
        }

        return storage;
    }

    writeStorage(date: string) {
        setTimeout(() => {
            this.remindersChanged.next(date);
        }, 20);
        localStorage.setItem('reminders', JSON.stringify(this.reminders));
    }

    dateFormat(date: Date): string {
        return date.getFullYear() + '-' + (date.getMonth() + 1);
    }
}