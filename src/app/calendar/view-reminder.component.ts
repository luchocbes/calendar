import { Component, Input, ViewChild, OnInit } from "@angular/core";
import { Reminder, City } from './types';
import { StorageService } from './storage.service';
import { CalendarComponent } from './calendar.component';
import { WeatherService } from './weather.service';

@Component({
    selector: 'view-reminder',
    templateUrl: 'view-reminder.component.html',
    styleUrls: ['view-reminder.component.scss']
})
export class ViewReminderComponent implements OnInit {
    @Input() data: Reminder;
    
    @ViewChild('reminderDeleteModal') reminderDeleteModal;

    weather: any;
    weatherError: boolean = false;

    constructor(private ss: StorageService, private ws: WeatherService, private cal: CalendarComponent) { }

    ngOnInit() {
        this.ws.getWeather(this.data.cityId).subscribe(
            w => { this.weather = w; this.weatherError = false; },
            e => { this.weatherError = true; }
        );}

    get cities(): City[] {
        return this.ws.getCities();
    }

    getCity(cityId: number): string {
        let cities = this.cities.filter((c: City) => c.id === cityId);
        return (cities.length) ? `${cities[0].name}, ${cities[0].country}` : '';
    }

    getWeather(what: string) {
        switch (what) {
            case 'icon' : 
                return (!this.weather) ? '' : `<img src="http://openweathermap.org/img/w/${this.weather['weather'][0]['icon']}.png" width="50" height="50" alt="" />`;

            case 'weather' : 
                if (this.weatherError) {
                    return '<em>Error fetching weather!</em>';
                } else if (this.weather) {
                    return `${this.weather['weather'][0]['description']}`;
                }

                return 'Loading...';
        }
    }
}