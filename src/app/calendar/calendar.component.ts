import { Component, OnInit, Input, OnChanges, SimpleChanges, ViewChild, OnDestroy, Renderer2 } from '@angular/core';
import { CreateEditModalComponent } from './create-edit.component';
import { Subscription } from 'rxjs';

import { WEEKDAYS, MONTHS, YEAR_MIN, YEAR_MAX } from './datetime.component';

import { StorageService } from './storage.service';
import { Reminder } from './types';

@Component({
    selector: 'calendar',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit, OnChanges, OnDestroy {
    @Input('month') currentMonth: number;
    @Input('year') currentYear: number;

    @ViewChild('deleteAllModal') deleteAllModal;
    @ViewChild('reminderViewModal') reminderViewModal;
    @ViewChild('reminderNewEditModal') reminderNewEditModal;
    @ViewChild(CreateEditModalComponent) reminderNewEditComponent: CreateEditModalComponent;
    @ViewChild('reminderDeleteModal') reminderDeleteModal;

    currentDate: Date;
    today: Date = new Date();

    minYear: number = YEAR_MIN;
    maxYear: number = YEAR_MAX;

    calendar: any[] = [];

    reminders: Object;

    reminderSubscriber: Subscription;

    ngOnInit() {
        this.today = new Date(this.today.getFullYear(), this.today.getMonth(), this.today.getDate(), 0, 0, 0);

        this.reminderSubscriber = this.ss.remindersChanged.subscribe(date => {
            if (date && date === this.date) {
                this.loadReminders();
            }
        });
    }

    ngOnChanges(changes: SimpleChanges) {
        if (
            (changes.currentMonth && changes.currentMonth.currentValue !== changes.currentMonth.previousValue) ||
            (changes.currentYear && changes.currentYear.currentValue !== changes.currentYear.previousValue)
        ) {
            this.init();
        }
    }

    ngOnDestroy(){
        this.reminderSubscriber.unsubscribe();
    }

    constructor(private ss: StorageService, private renderer: Renderer2) { }

    init() {
        this.reminders = undefined;

        if (!this.currentMonth || !this.currentYear) {
            this.currentMonth = this.currentDate.getMonth() + 1;
            this.currentYear = this.currentDate.getFullYear();
        }

        this.currentDate = new Date(this.currentYear, this.currentMonth - 1, 1);

        let totalDays = (new Date(this.currentYear, this.currentMonth, 0)).getDate();
        let firstDayWeek = this.currentDate.getDay();

        let weeks = (firstDayWeek + totalDays) / 7;
        if (!Number.isInteger(weeks)) {
            weeks = Math.floor(weeks + 1);
        }
        this.calendar = new Array(weeks);

        let fromDate: Date = new Date(this.currentYear, this.currentMonth - 1, 1);
        
        /**
         * If 1st day doesn't start on Sunday, we start calculating from previous month
         */
        let subtract = (firstDayWeek > 0) ? (firstDayWeek + 1) : 1;
        fromDate = new Date(fromDate.getTime() - (subtract * 24 * 60 * 60 * 1000));

        let week = -1;
        for (let day = 1; day <= (weeks * 7); day++) {
            if (day % 7 === 1) {
                week++;
                this.calendar[week] = [];
            }

            let dateTmp = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate()).getTime();
            let date = new Date(dateTmp + (day * 24 * 60 * 60 * 1000));

            let weather;

            this.calendar[week].push({ date, weather });
        }

        this.loadReminders();
    }

    loadReminders() {
        let remindersTmp = this.ss.getReminders(this.date);

        for (let rem in remindersTmp) {
            if (remindersTmp[rem]) {
                remindersTmp[rem].sort((a: Reminder, b: Reminder) => {
                    let aDate = a.date.getTime();
                    let bDate = b.date.getTime();
    
                    if (aDate < bDate) {
                        return -1;
                    } else if (aDate > bDate) {
                        return 1;
                    } else {
                        return 0;
                    }
                });
            }
            this.reminders = remindersTmp;
        }
    }

    get date(): string {
        return `${this.currentYear}-${this.currentMonth}`;
    }

    get days(): string[] {
        return WEEKDAYS; 
    };
    get months(): string[] {
        return MONTHS;
    };

    getDay(date: Date): number {
        return date.getDate();
    }

    isToday(date: Date): boolean {
        return (this.today.getTime() === date.getTime());
    }

    isOtherMonth(date: Date): boolean {
        return (this.currentDate.getMonth() !== date.getMonth());
    }

    isWeekend(date: Date): boolean {
        return [0, 6].indexOf(date.getDay()) !== -1;
    }

    checkYear() {
        if (this.currentYear < this.minYear || this.currentYear > this.maxYear) {
            this.currentYear = (new Date()).getFullYear();
        }

        this.init();
    }

    getReminders(day: number): Reminder[] {
        return ((this.reminders) ? this.reminders[day] : []) || [];
    }

    /* Delete Reminder Modal */
    reminderDeleteModalData = {
        buttons: [
            {
                buttonText: 'Delete',
                buttonClass: 'btn-danger',
                buttonCallback: () => {
                    this.deleteReminder(this.reminderViewModalData.data);
                    this.reminderDeleteModal.close();
                    this.reminderViewModalData.afterClose();
                }
            }, {
                buttonText: 'Cancel',
                buttonClass: 'btn-secondary',
                buttonCallback: () => this.reminderDeleteModal.close()
            }
        ],
        title: 'Delete Reminder'
    }

    deleteReminder(reminder: Reminder) {
        this.ss.removeReminder(reminder);
    }

    /* Edit Reminder */
    reminderViewModalData = {
        buttons: [
            {
                buttonText: 'Delete Reminder',
                buttonClass: 'btn-danger mr-auto',
                buttonCallback: () => this.reminderDeleteModal.open()
            }, {
                buttonText: 'Edit Reminder',
                buttonClass: 'btn-success',
                buttonCallback: () => {
                    this.reminderViewModal.close();
                    this.editReminder(this.reminderViewModalData.data);
                }
            }, {
                buttonText: 'Close',
                buttonClass: 'btn-secondary',
                buttonCallback: () => this.reminderViewModalData.afterClose()
            }
        ],
        afterClose: () => {
            this.reminderViewModalData.data = undefined;
            this.reminderViewModal.close();
        },
        title: 'Reminder',
        data: undefined
    }

    /* Edit Reminder */
    reminderNewEditModalData = {
        buttons: [
            {
                buttonText: 'Save',
                buttonClass: 'btn-primary',
                buttonCallback: () => {
                    if (this.reminderNewEditComponent.saveReminder()) {
                        this.reminderNewEditModalData.afterClose();
                        this.init();
                    }
                }
            }, {
                buttonText: 'Cancel',
                buttonClass: 'btn-secondary',
                buttonCallback: () => this.reminderNewEditModalData.afterClose()
            }
        ],
        afterClose: () => {
            this.reminderNewEditModalData.data = undefined;
            this.reminderNewEditModalData.action = undefined;
            this.reminderNewEditModal.close();
        },
        title: (): string => {
            if (this.reminderNewEditModalData.action === 'edit') {
                return 'Edit Reminder';
            } else {
                return 'New Reminder';
            }
        },
        action: undefined,
        data: undefined
    }

    viewReminder(reminder: Reminder) {
        this.reminderViewModalData.data = reminder;

        this.reminderViewModal.open();

        let modalHeader = this.reminderViewModal.a11yModal.nativeElement.querySelector('.modal-header');
        this.renderer.setStyle(modalHeader, 'background-color', this.reminderViewModalData.data.color);
    }

    addReminder() {
        this.reminderNewEditModalData.data = {};
        this.reminderNewEditModalData.action = 'new';
        this.reminderNewEditModal.open();
    }

    editReminder(reminder: Reminder) {
        this.reminderNewEditModalData.data = { ...reminder };
        this.reminderNewEditModalData.action = 'edit';
        this.reminderNewEditModal.open();
    }

    /* Delete All Reminders for a given Date */
    deleteAllModalData = {
        buttons: [
            {
                buttonText: 'Yes',
                buttonClass: 'btn-danger',
                buttonCallback: () => {
                    this.ss.deleteAll(this.deleteAllModalData.date);
                    this.deleteAllModalData.afterClose();
                }
            }, {
                buttonText: 'Cancel',
                buttonClass: 'btn-secondary',
                buttonCallback: () => this.deleteAllModalData.afterClose()
            }
        ],
        afterClose: () => {
            this.deleteAllModalData.date = undefined;
            this.deleteAllModal.close();
        },
        title: 'Delete All Reminders',
        date: undefined
    }

    deleteAll(date: Date) {
        this.deleteAllModalData.date = date;
        this.deleteAllModal.open();
    }
}