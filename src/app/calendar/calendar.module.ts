import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { CalendarComponent } from './calendar.component';
import { ReminderComponent } from './reminder.component';
import { ViewReminderComponent } from './view-reminder.component';
import { CreateEditModalComponent } from './create-edit.component';
import { DateTimeComponent } from './datetime.component';

import { A11yModal } from '../a11y/components/modal';

import { StorageService } from './storage.service';
import { WeatherService } from './weather.service';

@NgModule({
  declarations: [CalendarComponent, ReminderComponent, ViewReminderComponent, CreateEditModalComponent, DateTimeComponent],
  imports: [CommonModule, FormsModule, HttpClientModule, A11yModal],
  providers: [WeatherService, StorageService],
  exports: [CalendarComponent]
})
export class CalendarModule { }